import { Component, OnInit } from '@angular/core';
import { BottomSheetParams } from '@nativescript-community/ui-material-bottomsheet/angular';
import { Page } from '@nativescript/core/ui/page';

@Component({
  selector: 'ns-filters-modal',
  moduleId: module.id,
  templateUrl: './filters.modal.html',
})

export class FiltersModal implements OnInit {
    constructor(
      private params: BottomSheetParams,
      private page: Page,
    ) {}

    ngOnInit() {}

    close() {
      this.params.closeCallback([]);
    }
}
