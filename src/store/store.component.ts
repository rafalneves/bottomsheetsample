import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { BottomSheetOptions, BottomSheetService } from '@nativescript-community/ui-material-bottomsheet/angular';

import { FiltersModal } from './filters.modal';

@Component({
    selector: 'ns-store',
    moduleId: module.id,
    templateUrl: './store.component.html',
})

export class StoreComponent implements OnInit {
  constructor(
    private bottomSheet: BottomSheetService, 
    private viewContainerRef: ViewContainerRef,
  ) {
  }

  ngOnInit() {
  }

  open() {
    const options: BottomSheetOptions = {
      viewContainerRef: this.viewContainerRef,
      dismissOnDraggingDownSheet: false
    };

    this.bottomSheet.show(FiltersModal, options).subscribe(result => {
      if (result) {
        console.log(result);
      }
    });
  }
}
