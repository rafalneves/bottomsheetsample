import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { NativeScriptMaterialBottomSheetModule } from '@nativescript-community/ui-material-bottomsheet/angular';

import { StoreRoutingModule } from './store-routing.module';
import { StoreComponent } from './store.component';
import { FiltersModal } from './filters.modal';

@NgModule({
  imports: [
    NativeScriptCommonModule,
    StoreRoutingModule,
    NativeScriptMaterialBottomSheetModule.forRoot(),
  ],
  declarations: [
    StoreComponent,
    FiltersModal,
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})

export class StoreModule { }
